#!/usr/bin/perl
# 
# @author Sidharth Jha
#
use strict;
use modules::filecount qw(_numberOfLines);
use Cwd;

my $totalLines = 0;
my $filePath = 0;
my $linesInFile;
my $dirName;
my $fileORdir;
my @filePaths;
my $prevDirName="";
my $endTotal=-1;

print "\n<*COUNTER (Author:Sidharth Jha)*>\n";
print "Format- \'directory_name : file_name : #lines_in_file\'\n";
print "\n<------------------------BEGIN------------------------->\n";
&_countLines(@ARGV);
$endTotal += $totalLines;
print "<=============[TOTAL($prevDirName): $totalLines]================>\n";

print "\n<=============****[GRAND TOTAL COUNT: $endTotal]****=================>\n";
print "<-------------------------END-------------------------->\n\n";

sub _countLines{
	foreach(@_){
		$filePath = $_;
		if($filePath !~ /^*\/\*$/){
			$fileORdir = $filePath;
			@filePaths = split(/\//,$filePath);
			$filePath = pop @filePaths;
			$dirName = pop @filePaths;
			if(! -d $fileORdir){
				if(!($dirName eq $prevDirName || $endTotal == -1)){
					print "<=============[TOTAL($prevDirName): $totalLines]=================>\n";
					$endTotal += $totalLines;
					$totalLines = 0;
				}
				$prevDirName = $dirName;
				$linesInFile = _numberOfLines($_);
				print "$dirName: $filePath: \t$linesInFile\n";
				$totalLines += $linesInFile;
				if($endTotal == -1){$endTotal = 0;}
			}else{
				my @tmp = `find $fileORdir`;
				chomp(@tmp = @tmp);
				my @dirArr;
				foreach my $name (@tmp){
					if(-d $name){ $name .= '/*';}
					@dirArr = (@dirArr,$name);
				}	
				_countLines(@dirArr);
			}
		}
	}
}
