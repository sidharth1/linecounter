# README #
## Author: Sid ##
(Please see the end of the document for a sample output)

## 1. SETUP (Linux): ##
You essentially need to run the 'countline' shell script. You will need to set the appropriate permissions and create a symbolic link to the script in the '/usr/bin/' directory. Here are the steps:

```
#!shell


$ chmod +x countline main_perl.pl
$ cd /usr/bin
$ su
$ ln -s /path/to/downloaded/LineCounter/countline count
```


Exit superuser with Ctrl+D....
Done!

## 2. USAGE EXAMPLES ##

```
#!shell

$ count /path/to/file.pdf
$ count path_to_directory
$ count /path/to/file1.c  ../source.java /go/to/directory1/* ~/another/directory

```


##SAMPLE OUTPUT:##


```
#!shell

$ count /home/**blurred**/

<*COUNTER (Author:Sidharth Jha)*>
Format- 'directory_name : file_name : #lines_in_file'

<------------------------BEGIN------------------------->
generic: JsonResponseHandler.java: 	104
generic: SortTable.java: 	64
generic: EndpointParser.java: 	131
generic: Parser.java: 	105
<=============[TOTAL(generic): 404]=================>
identifiers: .ItemIdentifier.java.swp: 	11
identifiers: PriceIdentifier.java: 	67
identifiers: ItemIdentifier.java: 	73
<=============[TOTAL(identifiers): 151]=================>
workspace: Workspace.java: 	25
workspace: Items.java: 	221
workspace: WorkspaceFactory.java: 	30
workspace: Price.java: 	88
<=============[TOTAL(workspace): 364]=================>
rest: RestClient.java: 	117
<=============[TOTAL(rest): 117]================>

<=============****[GRAND TOTAL COUNT: 1036]****=================>
<-------------------------END-------------------------->

```

:)