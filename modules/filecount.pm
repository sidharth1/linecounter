#@author: Sidharth Jha
package modules::filecount;
use strict;
use Exporter qw(import);

our @EXPORT_OK = qw(_numberOfLines);

sub _numberOfLines{
	open(DATA,$_[0]);
	while(<DATA>){
	}
	my $linesRead = $.;
	close(DATA);
	return $linesRead;
}
